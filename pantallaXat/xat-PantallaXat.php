<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xat - Inici</title>
    <link rel="stylesheet" href="./css/style_Xat.css">
    <link rel="stylesheet" href="./emojis/emojibuttonlistjs.css" />
    <script defer type="text/javascript" src="./js/mostrarConver.js"></script>
    <script defer src="emojis/emojibuttonlistjs.js"></script>
    <script defer src="js/funcionsemoji.js"></script>
</head>
<body>
<?php
    
    include './php/arxiuText.php';
    $nom = $_GET['nombre'];

    echo "<div class='contactos'>";
        echo "<div class='tu'>";
            echo "<img src='./img/perfil.jpg'>";
            echo "<label>Hola, $nom</label><br><br>";
            echo "<a href='../editarPerfil/xatEditarPerfil.html' class='boto'><button>Editar Perfil</button></a>";
            echo "<a href='../tancarSessio/xatTancarSessio.html' class='boto'><button>Tancar sessio</button></a>";
        echo "</div>";

        echo "<div class='Conct'>";
            echo "<img src='./img/perfil.jpg'>";
            echo "<label>Contacte 1</label>";
        echo "</div>";
        echo "<div class='Conct'>";
            echo "<img src='./img/perfil.jpg'>";
            echo "<label>Contacte 2</label>";
        echo "</div>";
        echo "<div class='Conct'>";
            echo "<img src='./img/perfil.jpg'>";
            echo "<label>Contacte 3</label>";
        echo "</div>";
    echo "</div>";

    echo "<div class='xat'>";

        echo "<form method='POST'>";

            echo "<div class='encapcalat'>";
                echo "<img src='./img/perfil.jpg'>";
                echo "<label>Contacte 1</label>";
            echo "</div>";


            echo "<div class='conversa'><br>";

                $fh = fopen('./txt/log.txt','a');
                fclose($fh);

                $fr = fopen('./txt/log.txt','r');

                while ($line = fgets($fr)) {
                    echo "<p id='enviat'>$line</p>";
                }
                fclose($fr);
            
            echo "</div>";



            echo "<div class='missatge'>";

                echo "<input id='text1' type='text' name='msg' size='50' placeholder='Escriu aquí el teu missatge' required>";
                echo "<a href='' class='boto'><button type='submit'><img src='./img/enviar.png'></button></a>";
                echo "<button id='button1'><img src='./img/logoemj.png'></button>";
                
            echo "</div>";

        echo "</form>";
    echo "</div>";

      
?>

</body>
</html>